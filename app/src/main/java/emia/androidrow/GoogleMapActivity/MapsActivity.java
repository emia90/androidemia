package emia.androidrow.GoogleMapActivity;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import cz.msebera.android.httpclient.Header;
import emia.androidrow.ForeCastWeather.ForecastListAdapter;
import emia.androidrow.ForeCastWeather.Models.WeatherModels;
import emia.androidrow.R;
import emia.androidrow.Utils.BaseActivity;
import emia.androidrow.Utils.PublicMethods;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private GoogleMap mMap;
    TextView location,temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        location= findViewById(R.id.location);
        temp= findViewById(R.id.temp);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sematec = new LatLng(35.7588247, 51.3930307);

       // mMap.addMarker(new MarkerOptions().position(sematec).title("Marker in sematec"));
        mMap.setOnCameraIdleListener(this);
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                sematec, 18);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        mMap.animateCamera(location);

    }

void getWeatherByLocation(double lat, double lng){
    String url="https://api.darksky.net/forecast/4ffe069376ff8570e096f906edbe07cc/"+lat+","+lng+"";
    AsyncHttpClient asyncHttpClient= new AsyncHttpClient();
    asyncHttpClient.get(url, new TextHttpResponseHandler() {
        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Toast.makeText(MapsActivity.this,"Error Load Yahoo",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String responseString) {
            parseAndShowResult(responseString);

        }
    });


}
    private void parseAndShowResult(String response) {
        try {
            Gson gson= new Gson();
            WeatherModels wed=
                    gson.fromJson(response,WeatherModels.class);
            String tempi= Double.toString(wed.getCurrently().getTemperature());
            temp.setText(tempi);
            String loc= wed.getTimezone();
            location.setText(loc.substring(loc.lastIndexOf("/") + 1));


        }catch (Exception e){
            PublicMethods.toasr(this,"err in load data");
        }
    }



    @Override
    public void onCameraIdle() {
        double latitude = mMap.getCameraPosition().target.latitude;
        double longitute= mMap.getCameraPosition().target.longitude;
        getWeatherByLocation(latitude,longitute);

    }
}
