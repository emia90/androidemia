package emia.androidrow.ContactList;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.List;

import emia.androidrow.R;
import emia.androidrow.Utils.BaseActivity;

public class ContactActivity extends BaseActivity {

    ListView contactList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        contactList= findViewById(R.id.contactList);
        String name[]={
                "Ali",
                "Javad",
                "eli",
                "sarah",
                "reghayeh",
                "Afsgin",
                "karim",
                "kami",
                "kourosh",
                "Ali",
                "Javad",
                "eli",
                "sarah",
                "reghayeh",
                "Afsgin",
                "karim",
                "kami",
                "kourosh"

        };
        ContactAdapter adapter= new ContactAdapter(mContex, name);
        contactList.setAdapter(adapter);

    }
}
