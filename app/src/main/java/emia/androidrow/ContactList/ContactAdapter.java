package emia.androidrow.ContactList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import emia.androidrow.R;

public class ContactAdapter extends BaseAdapter{
    Context mContext;
    String name[];

    public ContactAdapter(Context mContext, String[] name) {
        this.mContext = mContext;
        this.name = name;
    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return name[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.user_contact_list,parent,false);
        TextView tx= v.findViewById(R.id.contactItem);
        tx.setText(name[position]);
        return v;
    }
}
