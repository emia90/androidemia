package emia.androidrow;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;

import emia.androidrow.Utils.BaseActivity;
import emia.androidrow.Utils.PublicMethods;

public class SharePreferenceActivity extends BaseActivity implements View.OnClickListener {
   EditText userName,userPass;
   TextView show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_preference);
        bind();
        LoadData();

    }

    void bind(){
        userName= findViewById(R.id.userName);
        userPass= findViewById(R.id.userPass);
        show= findViewById(R.id.showHawk);
        findViewById(R.id.saveHawk).setOnClickListener(this);
    }

    void LoadData(){
        String defU = PublicMethods.getData(this,"userName" , ""  );
        String defP= PublicMethods.getData(this,"userPass","");

        if (defU.length()>0){
            userName.setText(defU);
        }
        if (defP.length()>0){
            userPass.setText(defP);
        }


    }

    @Override
    public void onClick(View v) {
        String name= userName.getText().toString().trim();
        String pass= userPass.getText().toString().trim();
        PublicMethods.saveData(this,"userName",name);
        PublicMethods.saveData(this,"userPass",pass);

        userPass.setText("");
        userName.setText("");
        PublicMethods.toasr(this,"Save data in Mobile");

    }
}
