package emia.androidrow;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

public class VideoViewActivity extends AppCompatActivity {
    VideoView myvideo;
String url= "https://hw16.cdn.asset.aparat.com/aparat-video/cf380c338a0cd0b08cd5e4e7fa931af413307230-360p__45890.mp4";

BroadcastReceiver incomingCallReciver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);
        myvideo= findViewById(R.id.myVideo);
        checkPermission();
        myvideo.setMediaController(new MediaController(this));
        //from phone setVideoPath
        myvideo.setVideoURI(Uri.parse(url));
        myvideo.start();
        incomingCallReciver= new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (myvideo.isPlaying()) {
                    myvideo.pause();
                }
            }
        };
        IntentFilter callFilter= new IntentFilter("android.intent.action.PHONE_STATE\"");

        registerReceiver(incomingCallReciver,callFilter);
        //send broadcast
        Intent myCustomIntent= new Intent("pause_music");
        sendBroadcast(myCustomIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(incomingCallReciver);
    }

    void checkPermission(){
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.READ_PHONE_STATE).
                withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                })
                .check();
    }
}
