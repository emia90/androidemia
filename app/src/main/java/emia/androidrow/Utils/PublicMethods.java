package emia.androidrow.Utils;

import android.content.Context;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class PublicMethods {

    public static void toasr(Context mContex,String msg){
        Toast.makeText(mContex, msg, Toast.LENGTH_SHORT).show();
    }

    public static void saveData(Context mContex,String key, String value){
        Hawk.put(key,value);
       // PreferenceManager.getDefaultSharedPreferences(mContex).edit().putString(key,value).apply();
    }

    public static String getData(Context mContex,String key,String value){

        return Hawk.get(key,value);
      //  return PreferenceManager.getDefaultSharedPreferences(mContex).getString(key,value);
    }


    public static String convertUnixTime(long unixTime){
        final DateTimeFormatter formatter= DateTimeFormatter.ofPattern("EEE, d MMM yyyy");

        String formattedDtm= Instant.ofEpochSecond(unixTime).atZone(ZoneId.of("GMT-4")).format(formatter);

        return formattedDtm;
    }


}
