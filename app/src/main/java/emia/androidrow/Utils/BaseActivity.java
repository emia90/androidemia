package emia.androidrow.Utils;

import android.content.Context;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.orhanobut.hawk.Hawk;

public class BaseActivity extends AppCompatActivity {
     public Context mContex;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContex= this;

        Hawk.init(mContex).build();
    }
}
