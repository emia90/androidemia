package emia.androidrow;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import emia.androidrow.Utils.PublicMethods;

public class IncomingCallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        PublicMethods.toasr(context,"incoming call");
    }

}
