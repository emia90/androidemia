package emia.androidrow.DataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHandler extends SQLiteOpenHelper{
    String db_create = ""+
            "CREATE TABLE users( "+
            "id INTEGER PRIMARY KEY AUTOINCREMENT , "+
            "username TEXT , "+
            "email TEXT , "+
            "mobile TEXT"+
            ")";
    public DataBaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    public void addUser(String username, String email,String mobile){
        SQLiteDatabase db= this.getWritableDatabase();
        String addUserQuery= ""+
                "INSERT INTO users(username,email,mobile)   "+
                "VALUES('"+username+"','"+email+"','"+mobile+"')";
        db.execSQL(addUserQuery);
        db.close();
    }


  public String getUser(){
        String result= "";
        String usersQuary="SELECT username,email,mobile FROM users";
        SQLiteDatabase db= this.getReadableDatabase();
      Cursor cursor= db.rawQuery(usersQuary,null);
      while (cursor.moveToNext())
      {
          result += cursor.getString(0)+" "+cursor.getString(1)+" "+cursor.getString(2)+"\n";
      }

        return result;
  }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(db_create);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
