package emia.androidrow.DataBase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import emia.androidrow.R;
import emia.androidrow.Utils.PublicMethods;

public class DataBaseActivity extends AppCompatActivity {
    EditText name, addr, mobile;
    private DataBaseHandler db;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base);
        db= new DataBaseHandler(this,"elmira.db",null,1);
        bind();
    }

    private void bind() {
        name= findViewById(R.id.nameUser);
        addr= findViewById(R.id.addUser);
        mobile= findViewById(R.id.mobileUser);
        result= findViewById(R.id.resultDB);
        findViewById(R.id.saveUser).setOnClickListener(V->{
           // save(name.getText().toString(),addr.getText().toString(),mobile.getText().toString());
            saveBySugar(name.getText().toString(),addr.getText().toString(),mobile.getText().toString());

            loadBySugar();

        });


    }
    void load(){
        result.setText(db.getUser());
    }

    void saveBySugar(String name, String addr, String mobile){
        UserEntity user= new UserEntity(name,addr,mobile);
        user.save();
       cleanData();
        loadBySugar();
    }

    private void save(String name, String addr, String mobile) {
        db.addUser(name,addr,mobile);
        PublicMethods.toasr(this,"save data");
      cleanData();
        load();
    }

    void loadBySugar(){
        result.setText("");
        List<UserEntity> users= UserEntity.listAll(UserEntity.class);
        for (UserEntity user:users
             ) {
            result.append(user.getUsername()+""+
                    user.getMobile()+""+
                    user.getEmail()+"\n"
            );


        }

    }

    void cleanData(){
        this.name.setText("");
        this.addr.setText("");
        this.mobile.setText("");
    }


}
