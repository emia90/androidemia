package emia.androidrow.DataBase;

import com.orm.SugarRecord;

public class UserEntity extends SugarRecord<UserEntity>{
    String username,mobile, email;

    public UserEntity(String username, String mobile, String email) {
        this.username = username;
        this.mobile = mobile;
        this.email = email;
    }

    public UserEntity() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
