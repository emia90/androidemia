package emia.androidrow.UserDetail;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import emia.androidrow.R;
import emia.androidrow.Utils.BaseActivity;
import emia.androidrow.Utils.PublicMethods;

public class UserDetailActivity extends BaseActivity {
    TextView ip, isp, country, city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        findViewById(R.id.reload).setOnClickListener(V->{
            load();
        });
        bind();
        load();
    }
    void load(){
        String url="http://ip-api.com/json";
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toasr(mContex,"Err in recive data");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                pardeAndShowResult(responseString);
            }
        });
    }

    void pardeAndShowResult(String response){
        try {
            JSONObject js= new JSONObject(response);
            String ipVal= js.getString("query");
            String CountryVal= js.getString("country");
            String ispVal= js.getString("isp");
            String cityVal= js.getString("city");
            ip.setText(ipVal);
            isp.setText(ispVal);
            country.setText(CountryVal);
            city.setText(cityVal);

        }catch (Exception e){
            PublicMethods.toasr(mContex,"eroor");
        }


    }

    private void bind() {
        ip = findViewById(R.id.ip);
        isp = findViewById(R.id.isp);
        country = findViewById(R.id.country);
        city = findViewById(R.id.city);



    }

}