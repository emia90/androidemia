package emia.androidrow.UserDetail;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import emia.androidrow.R;
import emia.androidrow.Utils.BaseActivity;
import emia.androidrow.Utils.PublicMethods;

public class IMDBActivity extends BaseActivity {
    TextView director,year,title;
    EditText movie;
    ImageView poster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imdb);
        bind();
    }

    private void bind() {
        director= findViewById(R.id.director);
        year= findViewById(R.id.year);
        title= findViewById(R.id.title);
        poster= findViewById(R.id.poster);
        movie= findViewById(R.id.movie);
        findViewById(R.id.showMovie).setOnClickListener(V->{
            load(movie.getText().toString());

        });


    }
    void load(String movieName){
        String url= "http://www.omdbapi.com/?t="+movieName+"&apikey=2b2d4093";
        AsyncHttpClient asyncHttpClient= new AsyncHttpClient();
        asyncHttpClient.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toasr(mContex,"err in load IMDB");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndShowInformation(responseString);
            }
        });

    }

    private void parseAndShowInformation(String responseString) {
        try {
            JSONObject js= new JSONObject(responseString);
            String validation= js.getString("Response");
            if (validation.contains("True")){
                title.setText(js.getString("Title"));
                director.setText(js.getString("Director"));
                year.setText(js.getString("Year"));
                Glide.with(mContex).load(js.getString("Poster")).into(poster);

            }else{
                throw new Exception("Err in name of movie" );
            }


        }catch(Exception e){
            PublicMethods.toasr(mContex,"Err in load Data");
        }

    }
}
