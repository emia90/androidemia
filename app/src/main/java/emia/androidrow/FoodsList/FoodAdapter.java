package emia.androidrow.FoodsList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import emia.androidrow.R;

public class FoodAdapter extends BaseAdapter {
    Context mContex;
    List<FoodListModel> foods;

    public FoodAdapter(Context mContex, List<FoodListModel> foods) {
        this.mContex = mContex;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int position) {
        return foods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v= LayoutInflater.from(mContex).inflate(R.layout.foods_list_item,parent,false);
        TextView name= v.findViewById(R.id.nameFood);
        TextView type= v.findViewById(R.id.typeFood);
        TextView price= v.findViewById(R.id.priceFood);
        ImageView image = v.findViewById(R.id.foodImage);

        name.setText(foods.get(position).getName());
        type.setText(foods.get(position).getType());
        price.setText(foods.get(position).getPrice()+"");
        Glide.with(mContex).load(foods.get(position).getImage()).into(image);



        return v;
    }
}
