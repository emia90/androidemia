package emia.androidrow.FoodsList;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import emia.androidrow.R;
import emia.androidrow.Utils.BaseActivity;

public class FoodsListActivity extends BaseActivity {

    ListView foodList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foods_list);
        foodList= findViewById(R.id.foodList);

        FoodListModel pizza= new FoodListModel();
        pizza.setName("pizza");
        pizza.setType("Fast Food");
        pizza.setPrice(25000);
        pizza.setImage("https://www.google.com/imgres?imgurl=https%3A%2F%2Firancook.ir%2Fwp-content%2Fuploads%2F2012%2F10%2Fpizza-small.jpg&imgrefurl=https%3A%2F%2Firancook.ir%2F10%2F16%2Fpitza-makhlot%2F4051%2F&docid=-enwVR7b91rhPM&tbnid=5s3NnwL6_CLUiM%3A&vet=10ahUKEwifva7ghs_fAhXGlIsKHcmiAsYQMwhmKAAwAA..i&w=600&h=400&safe=active&client=firefox-b-ab&bih=786&biw=1600&q=%D9%BE%DB%8C%D8%AA%D8%B2%D8%A7&ved=0ahUKEwifva7ghs_fAhXGlIsKHcmiAsYQMwhmKAAwAA&iact=mrc&uact=8");

        FoodListModel Lazania= new FoodListModel();
        Lazania.setName("Lazania");
        Lazania.setType("Fast Food");
        Lazania.setPrice(30000);
        Lazania.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrpNUw3OmC6Aaq0CFlsUu2Jx5kgUfd8tENpudf1iej6anqYe_29A");

        FoodListModel hamberger= new FoodListModel();
        hamberger.setName("hamberger");
        hamberger.setType("Fast Food");
        hamberger.setPrice(25000);
        hamberger.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSzxGOO8boRdeGLRY-L7nyPNUSP3y5tWfAhiwn2a0fUg7oAkd-1g");

        FoodListModel chiken= new FoodListModel();
        chiken.setName("chiken Cricpy");
        chiken.setType("Fast Food");
        chiken.setPrice(15000);
        chiken.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiAPaoIWGY3uRg3ZpEHAFXof5L0mDUpaKqT9L5quy1wVMFAhUD");


        FoodListModel fesenJoon= new FoodListModel();
        fesenJoon.setName("fesenJoon");
        fesenJoon.setType("traditional");
        fesenJoon.setPrice(20000);
        fesenJoon.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzHxsp6PyNdLSsKmwRhCpqycFZykRVqwhcS8Dbp0Zi_UYNJkw7");

        FoodListModel ghormehSabzi= new FoodListModel();
        ghormehSabzi.setName("ghormehSabzi");
        ghormehSabzi.setType("traditional");
        ghormehSabzi.setPrice(18000);
        ghormehSabzi.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRH4b8IY_q4bFgvEHm7LEpZ-cf6i3WMLKqnzlaBZz_9d4M9m9Gvww");

        List<FoodListModel> foods= new ArrayList<>();
        foods.add(pizza);
        foods.add(Lazania);
        foods.add(hamberger);
        foods.add(fesenJoon);
        foods.add(chiken);
        foods.add(ghormehSabzi);
        foods.add(pizza);
        foods.add(Lazania);
        foods.add(hamberger);
        foods.add(fesenJoon);
        foods.add(chiken);
        foods.add(ghormehSabzi);

        FoodAdapter adapter = new FoodAdapter(mContex,foods);
        foodList.setAdapter(adapter);
    }
}
