package emia.androidrow.ForeCastWeather;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;

import cz.msebera.android.httpclient.Header;
import emia.androidrow.ForeCastWeather.Models.WeatherModels;
import emia.androidrow.R;
import emia.androidrow.Utils.BaseActivity;
import emia.androidrow.Utils.PublicMethods;

public class ForeCastWeather extends BaseActivity {
    EditText lat, lang;
    ListView forecats;
    TextView result;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fore_cast_weather);
        dialog= new ProgressDialog(this);
        dialog.setTitle("Loading...");
        dialog.setMessage("Please Wait for Server Response");
        bind();
    }

    void bind(){
        lat= findViewById(R.id.lat);
        lang= findViewById(R.id.lang);
        result= findViewById(R.id.result);
        forecats= findViewById(R.id.forecast);

        findViewById(R.id.weatherBtn).setOnClickListener(V->{
            Double langOut= Double.parseDouble(lang.getText().toString());
            Double latOut = Double.parseDouble(lat.getText().toString());
            getWeatherWithPosition(latOut,langOut);
        });

    }

    void getWeatherWithPosition(Double lat,Double lang){
        String url="https://api.darksky.net/forecast/4ffe069376ff8570e096f906edbe07cc/"+lat+","+lang+"";

        dialog.show();
        AsyncHttpClient darksky= new AsyncHttpClient();

        darksky.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toasr(mContex,"Cann't find url");
                String preResult= Hawk.get("lat",null);
                if (preResult!= null){
                    parseAndShowResult(preResult);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndShowResult(responseString);
                Hawk.put("lat",responseString);

            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
            }
        });


    }

    private void parseAndShowResult(String response) {
        try {
            Gson gson= new Gson();
            WeatherModels wed=
                    gson.fromJson(response,WeatherModels.class);

            result.setText("Todays Temp "+wed.getCurrently().getTemperature()+" in "+ wed.getTimezone());
            ForecastListAdapter adaptrer= new ForecastListAdapter(mContex,wed.getDaily().getData());
            forecats.setAdapter(adaptrer);


        }catch (Exception e){
            result.setText(e.getMessage());

        }
    }
}
