package emia.androidrow.ForeCastWeather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import emia.androidrow.ForeCastWeather.Models.Datum_;
import emia.androidrow.R;
import emia.androidrow.Utils.PublicMethods;

public class ForecastListAdapter extends BaseAdapter {
    Context mContext;
    List<Datum_> daily;

    public ForecastListAdapter(Context mContext, List<Datum_> daily) {
        this.mContext = mContext;
        this.daily = daily;
    }

    @Override
    public int getCount() {
        return daily.size();
    }

    @Override
    public Object getItem(int position) {
        return daily.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v= LayoutInflater.from(mContext).inflate(R.layout.forecast_daily_items,parent,false);
        TextView date= v.findViewById(R.id.date);
        TextView min= v.findViewById(R.id.min);
        TextView max= v.findViewById(R.id.max);
        TextView speed= v.findViewById(R.id.speed);
        ImageView icon= v.findViewById(R.id.icon);

        Datum_ currentData= daily.get(position);

        date.setText("Date: "+ PublicMethods.convertUnixTime(currentData.getTime())+" ");
        min.setText("MinTemp: "+currentData.getTemperatureLow());
        max.setText("MaxTemp: "+currentData.getTemperatureHigh());
        speed.setText("WindSpeed: "+currentData.getWindSpeed());

        if (currentData.getIcon().contains("wind")){
            Glide.with(mContext).load("https://findicons.com/files/icons/480/weather/128/wind_flag_storm.png").into(icon);
        }
        else if (currentData.getIcon().contains("snow")){
            Glide.with(mContext).load("https://findicons.com/files/icons/1786/oxygen_refit/128/weather_snow.png").into(icon);
        }
        else if (currentData.getIcon().contains("clear")){
            Glide.with(mContext).load("https://findicons.com/files/icons/1035/human_o2/128/weather_clear.png").into(icon);
        }

        else if (currentData.getIcon().contains("fog")){
            Glide.with(mContext).load("https://findicons.com/files/icons/480/weather/128/fog.png").into(icon);
        }
        else if (currentData.getIcon().contains("cloudy")){
            Glide.with(mContext).load("https://findicons.com/files/icons/2127/ultimate_gnome/128/stock_weather_cloudy.png").into(icon);
        }
        else if (currentData.getIcon().contains("rain")){
            Glide.with(mContext).load("https://findicons.com/files/icons/478/weather/128/rain.png").into(icon);
        }


        return v;
    }
}
